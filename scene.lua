--[ GIBBON JOYEUX ]--

----------------
-- [ MODULE ] --
----------------

local	scene = {
	compute = {}
}

----------------------------
 -- module ( FUNCTIONS ) --
----------------------------

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

function		scene.compute.to( self, table )
	local		anim
	local		sign, number
	local		absolute

	-- not computed yet --
	if table.state == nil then
		table.state = false
		-- make relative values absolute --
		for key, val in pairs( table.var ) do
			if type( val ) == "string" then
				sign = val:sub( 1, 1 )
				number = tonumber( val:sub( 2, -1 ) )
				if sign == '+' then
					table.var[ key ] = table.table[ key ] + number
				elseif sign == '-' then
					table.var[ key ] = table.table[ key ] - number
				end
			end
		end
		-- run animation --
		anim = flux.to( table.table, table.time, table.var )
		anim:oncomplete( function() 
			table.state = true
		end )
		if table.ease then
			anim:ease( table.ease )
		end
		-- floor values
		if table.floored then
			anim:onupdate( function()
				for key, _ in pairs( table.var ) do
					table.table[ key ] = math.floor( table.table[key] )
				end
			end )
		end
	end
end

function		scene.compute.set( self, table )
	local		key, val

	-- not computed yet --
	if table.state == nil then
		for key, val in pairs( table.var ) do
			table.table[ key ] = val
		end
		table.state = true
	end
end

function		scene.compute.delay( self, table )
	-- not computed yet --
	if table.state == nil then
		table.timer = 1 / 60
		if table.timer >= table.time then
			table.state = true
		else
			table.state = false
		end
	-- in progress --
	elseif table.state == false then
		table.timer = table.timer + ( 1 / 60 )
		if table.timer >= table.time then
			table.state = true
		end
	end
end

function		scene.compute.simultaneous( self, table )
	local		elem

	table.state = true
	for _, elem in ipairs( table.list ) do
		if elem.state ~= true then
			table.state = false
		end
		self.compute[ elem.type ]( self, elem )
	end
end

function		scene.compute.sequence( self, table )
	local		elem

	table.state = true
	for _, elem in ipairs( table.list ) do
		self.compute[ elem.type ]( self, elem )
		if elem.state ~= true then
			table.state = false
			break
		end
	end
end

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------
-- BUILD TABLE
--------------------------------------------------

function		scene.to( table, variables, time, ease, floored )
	return {
		type = "to",
		table = table,
		var = variables,
		time = time,
		ease = ease,
		floored = (floored == nil) and true or floored
	}
end

function		scene.set( table, variable )
	return {
		type = "set",
		table = table,
		var = variable
	}
end

function		scene.delay( time )
	return {
		type = "delay",
		time = time
	}
end

function		scene.simultaneous( ... )
	local		list
	local		object

	list = {}
	for _, object in ipairs( { ... } ) do
		list[ #list + 1 ] = object
	end
	return { type = "simultaneous", list = list }
end

function		scene.sequence( ... )
	local		list
	local		object

	list = {}
	for _, object in ipairs( { ... } ) do
		list[ #list + 1 ] = object
	end
	return { type = "sequence", list = list }
end

--------------------------------------------------
-- MAIN
--------------------------------------------------

function		scene:init( table )
	self.table = table
end

function		scene:update( dt )
	scene.compute[ self.table.type ]( self, self.table )
	flux.update( dt )
end

-------------------------
 -- module ( RETURN ) --
-------------------------
return scene
