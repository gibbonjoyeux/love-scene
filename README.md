# scene.lua

**scene.lua** is a small love2d library which aims to ease scene building for games.
It is paired with the flux tweening library (https://github.com/rxi/flux/).

The library is made to easily describe animations and play them.

It handle, variable tweening (can be used to move objects), variable setting and delays.
These events can be arranged in sequence or simulaneous lists.

## functions

Each of these functions returns a table that can be used in `scene.sequence()` and `scene.simultaneous()` (`scene.sequence()` and `scene.sequence()` themselves)

- `scene.to( table, variable_aims, time, [ease], [floored] )`
- `scene.set( table, variable_aims )`
- `scene.delay( time )`

- `scene.sequence( table1, table2, ... )`
- `scene.simultaneous( table1, table2, ... )`

By default, the `scene.to` floors tweened variables to avoid inter pixels positions but it can be deactivate by passing `false` as `floorer` paramater.


`scene.to()` handles both **absolute** and **relative** values. Absolute values can be set as simple numbers. Relative values should be set a strings with the desired sign (ex: "+5" or "-7").

## use it

```

flux = require "flux"
scene = require "scene"

my_scene_variables = {
	player_x = 0,
	player_y = 0,
	player_state = 0
}

function		love.load()
	-- This example scene is based on a sequence.
	-- It first starts with a delay of 3 seconds.
	-- Once the delay is finished, the simultaneous begins,
	--  moving the player from 0,0 to 30,20 using linear easing
	--  with different timings (x from 0 to 30 in 3 seconds, y from 0, to 20 in 2 seconds)
	-- CREATE SCENE TABLE
	local my_scene_table = scene.sequence(
		scene.delay( 3 ),
		scene.simultaneous(
			scene.to( my_scene_variables, {player_x = 30}, 3, "linear" ),
			scene.to( my_scene_variables, {player_y = "+20"}, 2, "linear" )
		)
	)
	-- INIT SCENE
	scene:init( my_scene_table )
end

function		love.update( dt )
	-- UPDATE SCENE
	scene:update( dt )
end

function		love.draw()
	--[[
		draw the player using:
		`my_scene_variables.player_x`
		`my_scene_variables.player_y`
	]]--
end

```
